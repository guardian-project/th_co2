#ifndef CO2SENSORS_H
#define CO2SENSORS_H

/**********************************************************************/
/***        Macro Definitions                                       ***/
/**********************************************************************/

#define CO2_SENSOR_TYPE_NONE                                    0
#define CO2_SENSOR_TYPE_MHZ19B                                  1
#define CO2_SENSOR_TYPE_AIRSENSE_S8                             2

/**********************************************************************/
/***        Function Declarations                                   ***/
/**********************************************************************/

/*
 * This function read the co2 value of an airsense S8 CO2 sensor.
 * @return CO2 value in case of successful reading. -1 in case of error 
 * while reading the values.
 * 
 */

int airsenseS8_read_co2(uint8_t *temp_buff, uint8_t max_buff_len);

/*
 * This function read the co2 value of a mhz19b CO2 sensor.
 * @return CO2 value in case of successful reading. -1 in case of error 
 * while reading the values.
 * 
 */
 
int mhz19b_read_co2(uint8_t *temp_buff, uint8_t max_buff_len);


/**********************************************************************/
/***        END OF FILE                                             ***/
/**********************************************************************/

#endif /* CO2SENSORS_H */
