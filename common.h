#ifndef COMMON_H
#define COMMON_H

/**********************************************************************/
/***        Macro Definitions                                       ***/
/**********************************************************************/

//#define DEBUG

#ifdef DEBUG
    #define DBG_OUTPUT_PORT Serial
    #define PRINT(...) Serial.print(__VA_ARGS__)
    #define PRINTF(...) Serial.printf(__VA_ARGS__)
    #define PRINTLN(...) Serial.println(__VA_ARGS__)

    #define PRINT_ARRAY(add, len)                                      \
    do {                                                               \
        int i;                                                         \
        for (i = 0 ; i < (len) ; i++) {                                \
            Serial.printf("%02x", (unsigned int)((uint8_t*)(add))[i]); \
        }                                                              \
        Serial.println();                                              \
    } while(0)

#else
    #define PRINT(...)
    #define PRINTF(...)
    #define PRINTLN(...)
    #define PRINT_ARRAY(add, len)

#endif /* DEBUG */

/**********************************************************************/
/***        END OF FILE                                             ***/
/**********************************************************************/

#endif /* COMMON_H */
