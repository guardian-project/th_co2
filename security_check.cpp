/**********************************************************************/
/***        Include files                                           ***/
/**********************************************************************/

#include <Arduino.h>

/* Crypto for sha1 & AES
 * Useful link for tests:
 * http://aes.online-domain-tools.com/
 */

#include <Hash.h>

/* ~/Arduino/libraries/arduino-crypto for AES256 */
#include <Crypto.h>

/**********************************************************************/
/***        Local Include files                                     ***/
/**********************************************************************/
//#define DEBUG

#include "common.h"
#include "eeprom.h"
#include "board_defs.h"

/**********************************************************************/
/***        Constants                                               ***/
/**********************************************************************/

/* AES */
static const uint8_t aes_key[32] = {};

static const uint8_t aes_iv[16] = {};

/**********************************************************************/
/***        Static Variables                                        ***/
/**********************************************************************/


/* Security_checker function vars */

static char aux[AES_BLOCK_SIZE] = {0};
static uint8_t hash[20] = {0};
static uint8_t cipher[AES_BLOCK_SIZE] = {0};

/**********************************************************************/
/***        AUX Functions                                           ***/
/**********************************************************************/

/*
 * fjperez@
 *
 * We calculate CRC 16 bits of an array.
 *
 * In case we want to calculate the CRC of two different non-continuous memory
 * zones, we can do serveral calls to this function and use the crc value
 * returned by the last call as the crc param of the next call.
 *
 * E.g.
 *
 * aux = Utils_calcCRC16ITTparcial(0xFFFF, zone1, sizeof(zone1));
 * auy = Utils_calcCRC16ITTparcial(aux, zone2, sizeof(zone2));
 * auz = Utils_calcCRC16ITTparcial(auy, zone3, sizeof(zone3));
 *
 * Note that the first call, the CRC param must be 0xFFFF.
 */

static uint16_t Utils_calcCRC16ITTparcial(uint16_t crc /* by default 0xFFFF */, uint8_t *datos, uint16_t len) {

    uint16_t i;
    uint8_t j;
    uint8_t bitValue;
    uint8_t c15;
    uint8_t data;

    for(i = 0; i < len; i++) {
        data = *datos++;

        for(j = 0; j < 8; j++) {
            bitValue = ((data >> (7 - j)) & 0x01);
            c15 = ((crc >> 15) & 0x0001);
            crc = crc << 1;

            if ((c15 ^ bitValue) == 1) {
                crc = crc ^ 0x1021;
            }
        }
    }

    return crc;
}

void security_check(void) {

    /*
     * Get uint8_t hash[20] from the serial number and the chipID.
     */

    /*
     * We print the serial number and also the ESP8266 Chip ID.
     */

    PRINT(F("[S] Serial Number en EEPROM es: "));
    PRINTLN(eeprom.sn);

    strncpy(aux, eeprom.sn, sizeof(eeprom.sn)-2);
    uint32_t chipid = ESP.getChipId();

    PRINT(F("[S] Chip ID es: "));
    PRINTLN(String(chipid, HEX));

    /*
     * We concatenate the chipID to the Serial Number.
     */

    sprintf(&aux[strlen(aux)], "%08lx", (uint32_t) chipid);

    PRINT(F("[S] SHA1-Arg: "));
    PRINTLN(aux);

    /*
     * We calculate a SHA1 from the SerialNumerChipID and store it in hash[20].
     */
    sha1(aux, hash);

    PRINT(F("[S] Hash: "));
    PRINT_ARRAY(hash, sizeof(hash));

    /*
     * We concatenate to the serialnumber+chipID the hash calculated above.
     */

    int i;
    for (i = 0 ; i < 20; i++) {

        sprintf(&aux[strlen(aux)], "%02x", (unsigned int) hash[i]);
    }

    uint16_t crc = Utils_calcCRC16ITTparcial(0xFFFF, (uint8_t*)aux, strlen(aux));
    PRINT(F("[S] CRC:"));
    PRINT_ARRAY(&crc, 2);

    /*
     * We concatenate the CRC
     */
    sprintf(&aux[strlen(aux)], "%04x", (unsigned int) crc);

    /*
     * We do the AES de-encription.
     */

    // AES(const uint8_t *key, const uint8_t *iv, AES_MODE mode, CIPHER_MODE cipherMode);
    AES aes(aes_key, aes_iv, AES::AES_MODE_256, AES::CIPHER_DECRYPT);
    aes.process(eeprom.bloque_aes256, cipher, sizeof(cipher));

    PRINT_ARRAY(aux, sizeof(aux));
    PRINT_ARRAY(cipher, sizeof(cipher));

    if (0 == memcmp(aux, cipher, sizeof(cipher))) {
        PRINTLN(F("[S] OK"));
    }
    else {
        Serial.println("[ERROR] SECURITY CHECK");
        while (1) {
          digitalWrite(GREEN_LED,HIGH);
          delay(50);
          digitalWrite(GREEN_LED,LOW);
          delay(50);
        }
    }

}

/**********************************************************************/
/***        END OF FILE                                             ***/
/**********************************************************************/
