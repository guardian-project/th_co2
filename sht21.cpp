/**********************************************************************/
/***        Include files                                           ***/
/**********************************************************************/

#include <Arduino.h>
#include <Wire.h>

/**********************************************************************/
/***        Local Include files                                     ***/
/**********************************************************************/

#include "sht21.h"

/**********************************************************************/
/***        AUX Functions                                           ***/
/**********************************************************************/

/**
 * Origen de esta función: contiki-esqueleto/rafa-dev/sht21_i2c.c
 * Comprueba el checksum de una trama.
 *
 * @param data              Los datos.
 * @param len               Longitud de los datos.
 * @param chk               El checksum con el que comparar.
 *
 * @return                  1 si los checksum son iguales o 0 en caso contrario.
 */

uint8_t SHT21_checkCRC(uint8_t *data, uint8_t len, uint8_t chk) {
    uint8_t crc, i, bit;

    crc = 0;

    for (i = 0; i < len; i++) {
        crc ^= data[i];
        for (bit = 8; bit > 0; bit--) {
            if (crc & 0x80)
                crc = (crc << 1) ^ 0x131;
            else
                crc = (crc << 1);
        }
    }

    if (crc != chk) return 0;

    return 1;
}

/*
 * This function was taken directly from https://github.com/markbeee/SHT21.git at SHT21.cpp.
 * Since the original library didn't do a CRC check, it was modified and implemented
 * here to use the function SHT21_checkCRC() by Rafa.
 *
 * @return Non-Zero in case of successful reading. Zero in case of Error while reading
 * the values.
 */

uint8_t SHT21_single_read(float *tem, float *hum) {

    uint16_t result;
    uint8_t read_bytes[2] = {0};
    uint8_t checksum;

    /*
     * We ask for the hum parameter.
     */
    {

        Wire.beginTransmission(SHT21_ADDRESS);
        Wire.write(TRIGGER_HUMD_MEASURE_NOHOLD);
        Wire.endTransmission();
        delay(100);

        int i;

        Wire.requestFrom(SHT21_ADDRESS, 3);
        for( i = 0 ; i < 100 ; i++) {
            if (Wire.available() >= 3) {
                break;
            }
            delay(1);

        }

        if (i == 100) {
            return 0;
        }

        // return result

        read_bytes[0] = Wire.read();
        read_bytes[1] = Wire.read();
        checksum = Wire.read();

        if (!SHT21_checkCRC(read_bytes, 2, checksum))
            return 0;

        result = (read_bytes[0] << 8);
        result += read_bytes[1];
        result &= ~0x0003;   // clear two low bits (status bits)
    }

    *hum = (-6.0 + 125.0 / 65536.0 * (float)(result));

    /*
     * We ask for the tem parameter.
     */
    {

        Wire.beginTransmission(SHT21_ADDRESS);
        Wire.write(TRIGGER_TEMP_MEASURE_NOHOLD);
        Wire.endTransmission();
        delay(100);

        int i;

        Wire.requestFrom(SHT21_ADDRESS, 3);

        for( i = 0 ; i < 100 ; i++) {
            if (Wire.available() >= 3) {
                break;
            }
            delay(1);

        }

        if (i == 100) {
            return 0;
        }

        // return result
        read_bytes[0] = Wire.read();
        read_bytes[1] = Wire.read();
        checksum = Wire.read();

        if (!SHT21_checkCRC(read_bytes, 2, checksum))
            return 0;

        result = (read_bytes[0] << 8);
        result += read_bytes[1];
        result &= ~0x0003;   // clear two low bits (status bits)
    }

    *tem = (-46.85 + 175.72 / 65536.0 * (float)(result));

    return 1;
}

uint8_t SHT21_read(float *tem, float *hum,uint8_t retries) {

    int i = 0;

    /*
     * We try to communicate with the sensor at most 5 times.
     * If in those 5 times we cannot obtain correct values
     * from the sensor, we skip and use the previously obtained
     * values.
     */

    for (i = 0 ; i < retries ; i++) {

        if (SHT21_single_read(tem,hum)) {
            return 1;
        }

        Serial.println("SHT21: CHKSUM ERROR");

    }

    return 0;
}

/**********************************************************************/
/***        END OF FILE                                             ***/
/**********************************************************************/
