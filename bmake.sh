# http://www.esp8266.com/wiki/doku.php?id=esp8266-module-family
#
#

#!/bin/bash

APPNAME=app_v1.3.1

#ARDUINO_ESP_BIN=../../../arduino-1.8.13/arduino
ARDUINO_ESP_BIN=../../../arduino-1.8.5/arduino-esp

# NOT LEGACY
if [ "$1" == "build" ] ; then
    BOARD="esp8266:esp8266:generic:eesz=4M3M"
    R1=$1
elif [ "$1" == "build_512" ] ; then
    BOARD="esp8266:esp8266:generic:eesz=1M512"
    R1="build"
fi

#LEGACY CORE (OVERRIDE)
BOARD="esp8266:esp8266:generic:FlashSize=4M1M"

MKSPIFFS=../../../arduino-1.8.5/portable/packages/esp8266/tools/mkspiffs/0.1.2/mkspiffs
BUILD_PATH=./build
ESPTOOLS_PATH=../../../arduino-1.8.5/portable/packages/esp8266/tools/esptool/0.4.9/esptool
ESPTOOL_PY_PATH=../../../tools/esptool/esptool.py
OUTPUT_BINARY_DIR="bin"
TEMP_DIR="bin"

#################################################################
# Functions
#################################################################

#args string_to_calc_crc invert_or_not_invert [optional crc init value. Default 0xFFFF]
#
# 1 = invert. Other value no invert.
# crc_init_value -> in hex or dec. i.e 0x1234, 234234, 2332 ...

function calc_crc16() {

    while read -r -d "" -n 1 ; do astring+=( "$REPLY" ) ; done <<< "$1"

    if [ "$#" == "3" ] ; then
        crc=$3
    else
        crc=0xFFFF
    fi

    cnt=${#1}

    for ((x=0;x<$cnt;x++)); do
        char=$(printf '%d' \'"${1:$x:1}")

        for ((j=0;j<8;j++)); do
            bitValue=$(( (($char >> (7 - j)) & 0x01) ))
            c15=$(( (($crc >> 15) & 0x0001) ))
            crc=$(( $crc << 1 ))
            crc=$(( $crc & 0xffff ))

            if [ $(($c15 ^ $bitValue)) == 1 ] ; then
                crc=$(($crc ^ 0x1021))
            fi

        done

    done

    crc=$(( $crc & 0xffff ))

    # if invert
    if [ "$2" == "1" ] ; then
        lsb=$(( $crc & 0xff ))
        msb=$(( ($crc >> 8) & 0xff ))
        crc=$(( ($lsb << 8) + $msb ))
    fi

    printf "%04x\n" $crc
}


#default behaviour

if [ "$#" == "0" ] ; then
    echo "Bash make for build firmware."
    echo "Basic usage:"
    echo "   bmake build: to build main app binary."
    echo "   bmake build_web: prepare, compress and create a binary spiffs for webpage resources."
    echo "   bmake build_web_512: prepare, compress and create a binary spiffs for webpage resources [SPIFFS size needed]"
    echo "   bmake ulfw usb_dev firmware_file.bin: upload binary file to board through ttyUSB dev."
    echo "   bmake ulweb usb_dev webpage.bin: upload webpage to df through ttyUSB dev."
    echo "   bmake ulweb_512 usb_dev webpage.bin: upload webpage to df through ttyUSB dev."
    echo "   bmake uleep usb_dev eep.bin: upload eeprom to df through ttyUSB dev."
    echo "   bmake uleep_512 usb_dev eep.bin: upload eeprom to df through ttyUSB dev."
    echo "   bmake clean: remove all bin files, build and tmp_web directories."
    echo "   bmake gen usb_dev SERIAL_NUMBER eeprom_default_cfg.bin: Generate eeprom bin file with factory serial number."

    exit
fi

if [ ! -d "$OUTPUT_BINARY_DIR" ]; then
    mkdir $OUTPUT_BINARY_DIR
fi

if [ "$R1" == "build" ] ; then
    echo "Building main app ..."
    mkdir $BUILD_PATH
    $ARDUINO_ESP_BIN --board $BOARD --preserve-temp-files --pref build.path=$BUILD_PATH --verify ./$APPNAME.ino
    cp $BUILD_PATH/0-dont-remove.h.bin $OUTPUT_BINARY_DIR/$APPNAME.bin
fi

if [ "$1" == "build_web" ] ; then
    echo "Building webpage resources..."
    rm -r tmp_web
    cp data tmp_web -r
    #purifycss here
    #mergin common mime type here
    echo "Compressing ... "
    find ./tmp_web -type f \( -name "*.js" -o -name "*.css" -o -name "*.eot" -o -name "*.svg" -o -name "*.ttf" -o -name "*.woff" \) -print -exec gzip {} \;
    echo "Creating bin file (mkspiffs) ...."
    $MKSPIFFS -c tmp_web -p 256 -b 8192 -s 1028096 $OUTPUT_BINARY_DIR/web_image.bin

fi

if [ "$1" == "build_web_512" ] ; then
    echo "Building webpage resources..."
    rm -r tmp_web
    cp data tmp_web -r
    #purifycss here
    #mergin common mime type here
    echo "Compressing ... "
    find ./tmp_web -type f \( -name "*.js" -o -name "*.css" -o -name "*.eot" -o -name "*.svg" -o -name "*.ttf" -o -name "*.woff" \) -print -exec gzip {} \;
    echo "Creating bin file (mkspiffs) ...."
    $MKSPIFFS -c tmp_web -p 256 -b 8192 -s 524288 $OUTPUT_BINARY_DIR/web_image.bin

fi

if [ "$1" == "ulfw" ] ; then
    if [ "${2:0:5}" == "/dev/" ] ; then

        if [ "$3" == "" ] ; then
            echo "Need to parse a bin file"
            exit
        fi

        if [ ! -f $3 ]; then
            echo "File $3 not found!"
            exit
        fi

        echo "Uploading fimrware through $2 ..."
        #$ESPTOOLS_PATH -vv -cd ck -cb 460800 -cp $2 -ca 0x00000 -cf $3
        $ESPTOOL_PY_PATH --baud 460800 --port $2 write_flash 0x0 $3
    else

        echo "Error: dev name seems wrong. i.e: /dev/ttyUSB0"
        exit
    fi
fi

if [ "$1" == "ulweb" ] ; then
    if [ "${2:0:5}" == "/dev/" ] ; then

        if [ "$3" == "" ] ; then
            echo "Need to parse a bin file"
            exit
        fi

        if [ ! -f $3 ]; then
            echo "File $3 not found!"
            exit
        fi

        echo "Uploading webpage binary data ..."
        #$ESPTOOLS_PATH -cd ck -cb 460800 -cp $2 -ca 0x300000 -cf $3
        $ESPTOOL_PY_PATH --baud 460800 --port $2 write_flash 0x300000 $3

    else

        echo "Error: dev name seems wrong. i.e: /dev/ttyUSB0"
        exit

    fi
fi

if [ "$1" == "ulweb_512" ] ; then
    if [ "${2:0:5}" == "/dev/" ] ; then

        if [ "$3" == "" ] ; then
            echo "Need to parse a bin file"
            exit
        fi

        if [ ! -f $3 ]; then
            echo "File $3 not found!"
            exit
        fi

        echo "Uploading webpage binary data ..."
        #$ESPTOOLS_PATH -cd ck -cb 460800 -cp $2 -ca 0x300000 -cf $3
        #$ESPTOOL_PY_PATH --baud 460800 --port $2 write_flash 0x300000 $3
        $ESPTOOL_PY_PATH --baud 460800 --port $2 write_flash 0x7B000 $3

    else

        echo "Error: dev name seems wrong. i.e: /dev/ttyUSB0"
        exit

    fi
fi

if [ "$1" == "uleep" ] ; then

    if [ "${2:0:5}" == "/dev/" ] ; then

        if [ "$3" == "" ] ; then
            echo "Need to parse a bin file"
            exit
        fi

        if [ ! -f $3 ]; then
            echo "File $3 not found!"
            exit
        fi

        echo "Uploading webpage binary data ..."
        #$ESPTOOLS_PATH -cd ck -cb 460800 -cp $2 -ca 0x300000 -cf $3
        $ESPTOOL_PY_PATH --baud 460800 --port $2 write_flash 0x3FB000 $3

    else

        echo "Error: dev name seems wrong. i.e: /dev/ttyUSB0"
        exit

    fi
fi

if [ "$1" == "uleep_512" ] ; then

    if [ "${2:0:5}" == "/dev/" ] ; then

        if [ "$3" == "" ] ; then
            echo "Need to parse a bin file"
            exit
        fi

        if [ ! -f $3 ]; then
            echo "File $3 not found!"
            exit
        fi

        echo "Uploading webpage binary data ..."
        #$ESPTOOLS_PATH -cd ck -cb 460800 -cp $2 -ca 0x300000 -cf $3
        $ESPTOOL_PY_PATH --baud 460800 --port $2 write_flash 0xFB000 $3

    else

        echo "Error: dev name seems wrong. i.e: /dev/ttyUSB0"
        exit

    fi
fi

if [ "$1" == "gen" ] ; then

    #################################################################
    # Calculate SECURITY Block (EEPROM 512 bytes)
    #################################################################

    # Trimming or expending serial number and store it into a binary file.
    sn=`echo -n $3 | cut -c1-19 | tr -d '\n'`

    echo -e "\e[92mSN:\e[1;4m$sn\e[0m"
    len=$( echo -n $sn | wc -c)
    counts=$((20 - $len))

    echo -n $sn > $TEMP_DIR/sn.bin
    dd if=/dev/zero of=$TEMP_DIR/sn.bin bs=1 seek=$len count=$counts &> /dev/null

    # Obtaining the Chip ID of the ESP8266
    hwmac=$($ESPTOOL_PY_PATH --port $2 read_mac)
    #echo $hwmac

    hwmac=$(echo $hwmac | cut -d ' ' -f 18)
    #####

    # Obtaining the Chip ID of the ESP8266
    chip_id=$($ESPTOOL_PY_PATH --port $2 chip_id)
    #echo $chip_id

    chip_id=$(echo $chip_id | cut -d ' ' -f 19 | cut -d 'x' -f 2)

    mhwmac=$(echo $hwmac | tr [a-z] [A-Z])
    mchip_id=$(echo $chip_id | tr [a-z] [A-Z])
    echo -e "\e[93mMAC:$mhwmac - CHIP_ID:0x$mchip_id\e[0m\n"

    #####

    resultado=$sn$chip_id

    sha1=$(echo -n $resultado | sha1sum | awk '{print $1}')
    echo -e "\e[93mSHA: $resultado -> $sha1\e[0m"

    resultado=$resultado$sha1

    crc=$(calc_crc16 $resultado)
    echo -e "\e[93mCRC:$crc\e[0m"
    resultado=$resultado$crc

    echo -n $resultado > $TEMP_DIR/plaintext.bin
    #hexdump -C $TEMP_DIR/plaintext.bin

    len=$(stat --printf="%s" $TEMP_DIR/plaintext.bin )
    counts=$((80 - $len))
    echo "PlainText len $len. Needs $counts to 80"

    echo -e "\e[93mComponiendo eeprom ... \e[0m"

    dd if=/dev/zero of=$TEMP_DIR/plaintext.bin bs=1 seek=$len count=$counts &> /dev/null
    openssl enc -aes-256-cbc -K f392ffea6d962a9f74de1cb34f4c5d2b6f0b3caf1ba9197942bea37e7277a4d6 -iv ea07a055893392edd0064b33aba2e81c -in $TEMP_DIR/plaintext.bin -out $TEMP_DIR/bloque_aes.bin -nopad
    cat $TEMP_DIR/sn.bin $TEMP_DIR/bloque_aes.bin $4 > $TEMP_DIR/${sn}_eep_area.bin
    rm $TEMP_DIR/sn.bin $TEMP_DIR/bloque_aes.bin $TEMP_DIR/plaintext.bin

fi

if [ "$1" == "clean" ] ; then
    rm $OUTPUT_BINARY_DIR/*.bin
    rm -r $BUILD_PATH
    rm -r tmp_web
fi
