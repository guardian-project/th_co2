/**********************************************************************/
/***        Include files                                           ***/
/**********************************************************************/
#include "board_defs.h"
#include "co2_sensors.h"

/**********************************************************************/
/***        Const                                                   ***/
/**********************************************************************/

const uint8_t MHZ19B_CMD_READ_CO2[9]       = {0xFF, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79}; // Read command
const uint8_t MHZ19B_CMD_CALIBRATE_ZERO[9] = {0xFF, 0x01, 0x87, 0x00, 0x00, 0x00, 0x00, 0x00, 0x78}; // Calibrate ZERO point
const uint8_t MHZ19B_CMD_ABC_ON[9]         = {0xFF, 0x01, 0x79, 0xA0, 0x00, 0x00, 0x00, 0x00, 0xE6}; // Enables Automatic Baseline Correction
const uint8_t MHZ19B_CMD_ABC_OFF[9]        = {0xFF, 0x01, 0x79, 0x00, 0x00, 0x00, 0x00, 0x00, 0x86}; // Disables Automatic Baseline Correction

/* SENSEAIR S8 COMMANDS IN A MODBUS PROTOCOL FRAME */

const uint8_t SENSEAIRS8_CMD_READ_CO2[8]   = {0xFE, 0x04, 0x00, 0x03, 0x00, 0x01, 0xD5, 0xC5};       // Read command


/**********************************************************************/
/***        Vars                                                    ***/
/**********************************************************************/

// JUST FOR DEBUG
#define DEBUG

#include "common.h"
#ifdef DEBUG
uint8_t dbg_buff_len;
uint8_t dbg_buff[200];
#endif

/**********************************************************************/
/***        Helper function                                         ***/
/**********************************************************************/

int blocking_read(int nbytes, uint8_t *buff, int buff_size, unsigned long timeout) {

  unsigned long init_time = millis();
  int rbytes = 0;

  while ( ((millis() - init_time) < timeout) ) {
      while (Serial.available() > 0) {
        buff[rbytes++] = Serial.read();
        if (rbytes == nbytes) { goto loop_exit; }
      }
  }

  loop_exit:

  return rbytes;

}

/**********************************************************************/
/***        AIRSENSE S8 Functions                                   ***/
/**********************************************************************/

uint16_t airsenseS8_calc_checksum(uint8_t *data, uint8_t len) {

  uint16_t crc = 0xFFFF;
  uint8_t i,j;

  for (i=0; i < len; i++) {

    crc ^= data[i];

    for (j=0;j<8;j++) {

      if ( (crc & 1) != 0) {
        crc >>= 1;
        crc ^= 0xA001;
      }
      else {
        crc >>= 1;
      }

    }

  }

  return crc;

}

/*
 * This function read the co2 value of an airsense S8 CO2 sensor.
 * @return CO2 value in case of successful reading. -1 in case of error
 * while reading the values.
 *
 */

int airsenseS8_read_co2(uint8_t *temp_buff, uint8_t max_buff_len) {

  Serial.flush();
  digitalWrite(CO2_UART_ON,LOW);
  delay(2);

  dbg_buff_len = 0;
  while (Serial.available() > 0) { dbg_buff[dbg_buff_len++] = Serial.read();  }

  Serial.write(SENSEAIRS8_CMD_READ_CO2,sizeof(SENSEAIRS8_CMD_READ_CO2));
  int reads = blocking_read(7,temp_buff,max_buff_len,100);

  delay(2);
  digitalWrite(CO2_UART_ON,HIGH);

  PRINT(F("[D] Serial flushed data: "));
  PRINTLN(dbg_buff_len);
  PRINT_ARRAY(dbg_buff,dbg_buff_len);

  if (reads == 7) {

    if ((temp_buff[6] * 256 + temp_buff[5]) == airsenseS8_calc_checksum(temp_buff,5)) {
      return (temp_buff[3]*256 + temp_buff[4]);
    }
    else {
        return -2;
    }

  }

  return -1;

}

/**********************************************************************/
/***        MHZ19B Functions                                        ***/
/**********************************************************************/

uint8_t mhz19b_calc_checksum(uint8_t *packet) {

  uint8_t checksum = 0;
  for (uint8_t i = 1; i < 8; i++) { checksum += packet[i]; }
  checksum = (0xff - checksum) + 1;
  return checksum;

}

/*
 * This function read the co2 value of a mhz19b CO2 sensor.
 * @return CO2 value in case of successful reading. -1 in case of error
 * while reading the values.
 *
 */

int mhz19b_read_co2(uint8_t *temp_buff, uint8_t max_buff_len) {

  Serial.flush();
  digitalWrite(CO2_UART_ON,LOW);
  delay(2);

  while (Serial.available() > 0) { Serial.read();  }

  Serial.write(MHZ19B_CMD_READ_CO2,sizeof(MHZ19B_CMD_READ_CO2));
  int reads = blocking_read(9,temp_buff,max_buff_len,1000);

  delay(2);
  digitalWrite(CO2_UART_ON,HIGH);

  if ( (reads == 9) && (temp_buff[8] == mhz19b_calc_checksum(temp_buff)) ) {
    return (temp_buff[2]*256 + temp_buff[3]);
  }
  else {
    return -2;
  }

  return -1;

}
