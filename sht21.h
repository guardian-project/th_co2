#ifndef SHT21_H
#define SHT21_H

/**********************************************************************/
/***        Macro Definitions                                       ***/
/**********************************************************************/

#define SHT21_SENSOR_VCC_PIN D3

/* SHT21 related defines */
#define SHT21_ADDRESS 0x40  //I2C address for the sensor
#define TRIGGER_TEMP_MEASURE_NOHOLD  0xF3
#define TRIGGER_HUMD_MEASURE_NOHOLD  0xF5

/**********************************************************************/
/***        Function Declarations                                   ***/
/**********************************************************************/

/*
 * This function was taken directly from https://github.com/markbeee/SHT21.git at SHT21.cpp.
 * Since the original library didn't do a CRC check, it was modified and implemented
 * here to use the function SHT21_checkCRC() by Rafa.
 *
 * @return Non-Zero in case of successful reading. Zero in case of Error while reading
 * the values.
 */

uint8_t SHT21_single_read(float *tem, float *hum);

uint8_t SHT21_read(float *tem, float *hum,uint8_t retries);

/**********************************************************************/
/***        END OF FILE                                             ***/
/**********************************************************************/

#endif /* SHT21_H */
