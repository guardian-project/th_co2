var json_cfg;
function getCfg() {
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", "/conf.json", false);
    rawFile.onreadystatechange = function () {
      if (rawFile.readyState === 4) {
            if (rawFile.status === 200 || rawFile.status == 0) {
                json_cfg = JSON.parse(rawFile.responseText);
            }
        }
    }
    rawFile.send(null);
}
